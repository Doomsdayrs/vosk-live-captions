# VOSK Live Captioning
By Doomsdayrs
## Premise
Using the VOSK Speech to Text library, create a open source competitor to Google Live Captioning.

## Requirements
- Floating bubble containing generated text from speech
- Can target multiple languages
- Dedicated to my loving partner
- In-app updater for standalone version

## Constraints
- Only for Android

## Implied Requirements
- VOSK model management
- Android Accessibility Service